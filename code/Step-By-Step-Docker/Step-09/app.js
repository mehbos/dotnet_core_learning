var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello from node in docker-compose');
}).listen(3000);

console.log('Server is listening for incoming connections!');