@ECHO off
ECHO Testing node.js app

docker run -it -d --rm --name node_app -v %CD%:/src -w /src -p 8080:3000 node:12.10.0-alpine node app.js
