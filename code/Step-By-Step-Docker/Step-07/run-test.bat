@ECHO off
ECHO Testing node.js app

docker build -t node-app .

docker run -it -d --rm --name node_app -p 8080:3000 node-app
