using System;
using System.Buffers;
using System.Collections.Generic;

namespace Dotnet.Core.Learning.WorkingWithSequence
{
    #region ReadOnlySequenceSegmentImplementation
    public class SequenceSegment<T> : ReadOnlySequenceSegment<T>
    {
        public static (SequenceSegment<T> First, SequenceSegment<T> Last) FromCollection(IEnumerable<Memory<T>> collection)
        {
            SequenceSegment<T> result = null;
            SequenceSegment<T> last = null;
            foreach (var item in collection)
            {
                if (result == null)
                {
                    last = result = new SequenceSegment<T>(item);
                }
                else
                {
                    last = new SequenceSegment<T>(item, last);
                }
            }
            return (result, last);
        }
        public static (SequenceSegment<T> First, SequenceSegment<T> Last) FromCollection(params Memory<T>[] collection)
        {
            return FromCollection((IEnumerable<Memory<T>>)collection);
        }

        public static ReadOnlySequence<T> CreateSequence(IEnumerable<Memory<T>> collection)
        {
            (SequenceSegment<T> first, SequenceSegment<T> last) = FromCollection(collection);
            return new ReadOnlySequence<T>(first, 0, last, last.Memory.Length);
        }
        public static ReadOnlySequence<T> CreateSequence(params Memory<T>[] collection)
        {
            return CreateSequence((IEnumerable<Memory<T>>)collection);
        }

        public SequenceSegment(Memory<T> memory, SequenceSegment<T> prev = null)
        {
            Memory = memory;
            if (prev != null)
            {
                prev.Next = this;
                RunningIndex = prev.RunningIndex + prev.Memory.Length;
            }
        }
    }
    #endregion
}