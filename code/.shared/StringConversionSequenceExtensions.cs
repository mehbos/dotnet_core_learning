using System;
using System.Buffers;
using System.Runtime.InteropServices;
using System.Text;

namespace Dotnet.Core.Learning
{
    public static class StringConversionSequenceExtensions
    {
        public static int GetCharCount(this in ReadOnlySequence<byte> sequence, Encoding encoding, ref Decoder decoder)
        {
            checked
            {
                if (encoding.IsSingleByte)
                {
                    return (int)sequence.Length;
                }

                if (encoding is UnicodeEncoding)
                {
                    return (int)(sequence.Length >> 1);
                }

                if (encoding is UTF32Encoding)
                {
                    return (int)(sequence.Length >> 2);
                }
            }

            if (sequence.IsSingleSegment)
            {
                ReadOnlySpan<byte> span = sequence.First.Span;
                return span.IsEmpty ? 0 : encoding.GetCharCount(span);
            }

            int charCount = 0;
            if (decoder is null)
            {
                decoder = encoding.GetDecoder();
            }
            else
            {
                decoder.Reset();
            }
            foreach (ReadOnlyMemory<byte> segment in sequence)
            {
                ReadOnlySpan<byte> span = segment.Span;
                if (span.IsEmpty)
                {
                    continue;
                }

                charCount += decoder.GetCharCount(span, false);
            }
            return charCount;
        }
        public static int GetCharCount(this in ReadOnlySequence<byte> sequence, Encoding encoding)
        {
            Decoder temp = null;
            return GetCharCount(sequence, encoding, ref temp);
        }
        public static int GetCharCount(this in ReadOnlySequence<byte> sequence) => GetCharCount(sequence, Encoding.UTF8);

        private static int GetString(in ReadOnlySequence<byte> sequence, Span<char> chars, out int charsRead,
            Encoding encoding, Decoder decoder, bool consumeEntireBuffer)
        {
            if (chars.IsEmpty)
            {
                charsRead = 0;
                return 0;
            }

            if (consumeEntireBuffer && sequence.IsSingleSegment && decoder is null)
            {
                ReadOnlySpan<byte> buffer = sequence.First.Span;
                // we need to be sure we have enough space in the output buffer
                if (chars.Length >= encoding.GetMaxCharCount(buffer.Length)   // worst-case is fine
                    || chars.Length >= encoding.GetCharCount(buffer))         // *actually* fine
                {
                    charsRead = encoding.GetChars(buffer, chars);
                    return buffer.Length;
                }
            }

            if (decoder is null)
            {
                decoder = encoding.GetDecoder();
            }
            else
            {
                decoder.Reset();
            }

            int totalBytes = 0;
            charsRead = 0;
            bool isComplete = true;
            foreach (ReadOnlyMemory<byte> segment in sequence)
            {
                ReadOnlySpan<byte> buffer = segment.Span;
                if (buffer.IsEmpty)
                {
                    continue;
                }

                decoder.Convert(buffer, chars, false, out int bytesUsed, out int charsUsed, out isComplete);
                totalBytes += bytesUsed;
                charsRead += charsUsed;
                chars = chars.Slice(charsUsed);
                if (chars.IsEmpty)
                {
                    break;
                }
            }

            if (consumeEntireBuffer)
            {
                if (!isComplete || totalBytes != sequence.Length)
                {
                    throw new InvalidOperationException("Incomplete decoding frame");
                }
            }

            return totalBytes;
        }
        private static string GetString(in ReadOnlySequence<byte> sequence, int charCount, out int totalBytes,
            Encoding encoding, Decoder decoder, bool consumeEntireBuffer)
        {
            if (charCount == 0)
            {
                totalBytes = 0;
                return "";
            }

            string s = new string((char)0, charCount);
            // If receive an error in this line include TestUtils.cs in your project
            Span<char> chars = s.GetSpan();
            // Write this sequence to the retrieved memory
            totalBytes = GetString(sequence, chars, out int charsRead, encoding, decoder, consumeEntireBuffer);
            System.Diagnostics.Debug.Assert(charsRead == charCount);
            return s;
        }
        private static string GetString(in ReadOnlySequence<byte> sequence,
            Encoding encoding, Decoder decoder, bool consumeEntireBuffer)
        {
            if (sequence.IsEmpty)
            {
                return "";
            }
            if (sequence.IsSingleSegment)
            {
                ReadOnlySpan<byte> span = sequence.First.Span;
                return encoding.GetString(span);
            }

            int charCount = sequence.GetCharCount(encoding, ref decoder);
            string s = GetString(sequence, charCount, out _, encoding, decoder, consumeEntireBuffer);
            return s;
        }
        public static string GetString(this in ReadOnlySequence<byte> sequence, Encoding encoding = null, bool consumeEntireBuffer = true)
        {
            if (encoding is null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return GetString(sequence, encoding, null, consumeEntireBuffer);
        }
    }
}
