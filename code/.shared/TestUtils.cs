using System;
using System.Runtime.InteropServices;

namespace Dotnet.Core.Learning
{
    public static class TestUtils
    {
        private const string RandomAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly Random Generator = new Random();

        public static Memory<char> GetMemory(this string s)
        {
            ReadOnlyMemory<char> readonlyMemory = s.AsMemory();
            Memory<char> memory = MemoryMarshal.AsMemory(s.AsMemory());
            return memory;
        }
        public static Span<char> GetSpan(this string s)
        {
            return s.GetMemory().Span;
        }

        public static string GetRandomString(int length, ReadOnlySpan<char> alphabet, Random random = null)
        {
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }
            if (alphabet.IsEmpty)
            {
                throw new ArgumentException("Alphabet can't be empty", nameof(alphabet));
            }

            if (random is null)
            {
                random = Generator;
            }

            string s = new string((char)0, length);
            Span<char> buffer = s.GetSpan();
            for (int i = 0; i < length; i++)
            {
                int index = random.Next(alphabet.Length);
                buffer[i] = alphabet[index];
            }
            return s;
        }
        public static string GetRandomString(int length)
        {
            return GetRandomString(length, RandomAlphabet.AsSpan());
        }
    }
}