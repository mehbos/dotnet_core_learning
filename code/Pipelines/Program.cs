﻿using System;
using System.Buffers;
using System.IO;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace Dotnet.Core.Learning.WorkingWithPipelines
{
    class Program
    {
        #region Code
        public static void ProcessLine(ReadOnlySequence<byte> sequence)
        {
            // convert this content to string

            string line = sequence.GetString(Encoding.UTF8);
            Console.WriteLine("'{0}'", line);
            Console.WriteLine("----");
        }

        public static Task ProcessLinesAsync(Stream stream)
        {
            var pipe = new Pipe();
            Task writing = FillPipeAsync(stream, pipe.Writer);
            Task reading = ReadPipeAsync(pipe.Reader);

            return Task.WhenAll(reading, writing);
        }

        public static async Task FillPipeAsync(Stream stream, PipeWriter writer)
        {
            const int minimumBufferSize = 512;

            while (true)
            {
                // Allocate at least 512 bytes from the PipeWriter
                Memory<byte> memory = writer.GetMemory(minimumBufferSize);
                try
                {
                    // This extension method use Memory<byte> and return ValueTask instead of Task.
                    int bytesRead = await stream.ReadAsync(memory);
                    if (bytesRead == 0)
                    {
                        break;
                    }
                    // Tell the PipeWriter how much was read from the Stream.
                    writer.Advance(bytesRead);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR: {0}", ex);
                    break;
                }

                // Make the data available to the PipeReader
                FlushResult result = await writer.FlushAsync();

                if (result.IsCompleted)
                {
                    break;
                }
            }

            // Tell the PipeReader that there's no more data coming
            writer.Complete();
        }

        public static async Task ReadPipeAsync(PipeReader reader)
        {
            while (true)
            {
                ReadResult result = await reader.ReadAsync();

                ReadOnlySequence<byte> buffer = result.Buffer;
                SequencePosition? position = null;

                do
                {
                    // Look for a EOL in the buffer
                    position = buffer.PositionOf((byte)'\n');

                    if (position != null)
                    {
                        ReadOnlySequence<byte> slice = buffer.Slice(0, position.Value);
                        if (slice.Length != 0 &&
                            slice.Slice(slice.Length - 1).First.Span[0] == '\r')
                        {
                            slice = buffer.Slice(0, slice.Length - 1);
                        }

                        // Process the line
                        ProcessLine(slice);

                        // Skip the line + the \n character (basically position)
                        buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
                    }
                }
                while (position != null);

                // Stop reading if there's no more data coming
                if (result.IsCompleted && !buffer.IsEmpty)
                {
                    ProcessLine(buffer);
                    break;
                }

                // Tell the PipeReader how much of the buffer we have consumed
                reader.AdvanceTo(buffer.Start, buffer.End);
            }

            // Mark the PipeReader as complete
            reader.Complete();
        }

        public static async Task Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            using (var stream = new MemoryStream())
            {
                using (var writer = new StreamWriter(stream, Encoding.UTF8, 4096, true))
                {
                    writer.WriteLine(TestUtils.GetRandomString(1000));
                    writer.WriteLine("This is a test");
                    writer.WriteLine(TestUtils.GetRandomString(578));
                    writer.WriteLine("With multiple lines");
                    writer.WriteLine();
                    writer.WriteLine(TestUtils.GetRandomString(723, "ابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی"));
                    writer.Write("Last line");
                }

                stream.Position = 0;
                await ProcessLinesAsync(stream);
            }
        }
        #endregion
    }
}
