using System;
using System.Buffers;
using System.IO;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace Dotnet.Core.Learning.ValueTaskTests
{
    [MemoryDiagnoser]
    public class TaskVsValueTask
    {
        public abstract class SampleBase : IDisposable
        {
#pragma warning disable IDE0069 // Disposable fields should be disposed
            protected readonly Stream Stream;
#pragma warning restore IDE0069 // Disposable fields should be disposed
            protected readonly IMemoryOwner<byte> MemoryOwner;
            protected int DataProcessed;
            protected int DataRead;
            protected SampleBase(Stream stream, int bufferSize)
            {
                Stream = stream;
                MemoryOwner = MemoryPool<byte>.Shared.Rent(bufferSize);
            }

            public void Dispose()
            {
                MemoryOwner.Dispose();
            }
            protected bool HasData => DataProcessed < DataRead;
            protected byte ReadData()
            {
                return MemoryOwner.Memory.Span[DataProcessed++];
            }

            public static TaskSample<T> CreateTaskSample<T>(Stream stream, int bufferSize, Func<bool, byte, T> generator)
            {
                return new TaskSample<T>(stream, bufferSize, generator);
            }
            public static ValueTaskSample<T> CreateValueTaskSample<T>(Stream stream, int bufferSize, Func<bool, byte, T> generator)
            {
                return new ValueTaskSample<T>(stream, bufferSize, generator);
            }
        }

        public class ValueTaskSample<T> : SampleBase
        {
            private readonly Func<bool, byte, T> _generator;
            public ValueTaskSample(Stream stream, int bufferSize, Func<bool, byte, T> generator) : base(stream, bufferSize)
            {
                _generator = generator;
            }

            public async ValueTask<T> ReadAsync()
            {
                if (DataProcessed < DataRead)
                {
                    return _generator(true, ReadData());
                }

                DataRead = await Stream.ReadAsync(MemoryOwner.Memory);
                if (DataRead == 0)
                {
                    return _generator(false, 0);
                }

                DataProcessed = 0;
                return _generator(true, ReadData());
            }
        }
        public class TaskSample<T> : SampleBase
        {
            private readonly Func<bool, byte, T> _generator;
            public TaskSample(Stream stream, int bufferSize, Func<bool, byte, T> generator) : base(stream, bufferSize)
            {
                _generator = generator;
            }

            public async Task<T> ReadAsync()
            {
                if (DataProcessed < DataRead)
                {
                    return _generator(true, ReadData());
                }

                DataRead = await Stream.ReadAsync(MemoryOwner.Memory);
                if (DataRead == 0)
                {
                    return _generator(false, 0);
                }

                DataProcessed = 0;
                return _generator(true, ReadData());
            }
        }

        [Params(100 * 1024/*, 100 * 1024 * 1024*/)]
        public int StreamSize { get; set; }
        [Params(10, 100, 1000)]
        public int BufferSize { get; set; }

        public MemoryStream Stream { get; set; }

        [GlobalSetup]
        public void Setup()
        {
            byte[] buffer = new byte[1];
            Stream = new MemoryStream(StreamSize);
            for (int i = 0; i < StreamSize; i++)
            {
                Stream.Write(buffer, 0, 1);
                ++buffer[0];
            }
        }

        public static class TestWithValueTuple
        {
            public readonly static Func<bool, byte, (bool DataRead, byte Value)> Generator = (dataRead, value) => (dataRead, value);
            public readonly static Func<(bool DataRead, byte Value), bool> DataRead = v => v.DataRead;
            public readonly static Func<(bool DataRead, byte Value), int> GetValue = v => v.Value;

        }
        public static class TestWithByte
        {
            public readonly static Func<bool, byte, int> Generator = (dataRead, value) => dataRead ? (int)value : -1;
            public readonly static Func<int, bool> DataRead = v => v != -1;
            public readonly static Func<int, int> GetValue = v => v;
        }
        public static class TestWithInt
        {
            public readonly static Func<bool, byte, int> Generator = (dataRead, value) => dataRead ? (int)(value + 256) * 2 : -1;
            public readonly static Func<int, bool> DataRead = v => v != -1;
            public readonly static Func<int, int> GetValue = v => v;
        }

        [Benchmark]
        public Task<long> ValueTaskWithValueTupleTest()
        {
            return executeValueTaskTest(TestWithValueTuple.Generator, TestWithValueTuple.DataRead, TestWithValueTuple.GetValue);
        }
        [Benchmark]
        public Task<long> TaskWithValueTupleTest()
        {
            return executeTaskTest(TestWithValueTuple.Generator, TestWithValueTuple.DataRead, TestWithValueTuple.GetValue);
        }

        [Benchmark]
        public Task<long> ValueTaskWithByteTest()
        {
            return executeValueTaskTest(TestWithByte.Generator, TestWithByte.DataRead, TestWithByte.GetValue);
        }
        [Benchmark]
        public Task<long> TaskWithByteTest()
        {
            return executeTaskTest(TestWithByte.Generator, TestWithByte.DataRead, TestWithByte.GetValue);
        }

        [Benchmark]
        public Task<long> ValueTaskWithIntTest()
        {
            return executeValueTaskTest(TestWithInt.Generator, TestWithInt.DataRead, TestWithInt.GetValue);
        }
        [Benchmark]
        public Task<long> TaskWithIntTest()
        {
            return executeTaskTest(TestWithInt.Generator, TestWithInt.DataRead, TestWithInt.GetValue);
        }

        private async Task<long> executeTaskTest<T>(Func<bool, byte, T> generator, Func<T, bool> dataRead, Func<T, int> getData)
        {
            long result = 0;
            Stream.Position = 0;
            using (var sample = SampleBase.CreateTaskSample(Stream, BufferSize, generator))
            {
                while (true)
                {
                    T value = await sample.ReadAsync();
                    if (!dataRead(value))
                    {
                        break;
                    }

                    unchecked
                    {
                        result += getData(value);
                    }
                }
            }

            return result;
        }
        private async Task<long> executeValueTaskTest<T>(Func<bool, byte, T> generator, Func<T, bool> dataRead, Func<T, int> getData)
        {
            long result = 0;
            Stream.Position = 0;
            using (var sample = SampleBase.CreateValueTaskSample(Stream, BufferSize, generator))
            {
                while (true)
                {
                    T value = await sample.ReadAsync();
                    if (!dataRead(value))
                    {
                        break;
                    }

                    unchecked
                    {
                        result += getData(value);
                    }
                }
            }

            return result;
        }
    }
}
