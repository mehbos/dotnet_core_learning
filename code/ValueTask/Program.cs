﻿using System;
using System.Threading.Tasks;

namespace Dotnet.Core.Learning.ValueTaskTests
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkDotNet.Reports.Summary summary = BenchmarkDotNet.Running.BenchmarkRunner.Run<TaskVsValueTask>();

            Console.WriteLine("Test completed!");
        }
    }
}
