using System;

namespace Dotnet.Core.Learning.ValueTaskTests
{
    public static class Extensions
    {
        public static T PopFront<T>(this ref Memory<T> memory)
        {
            T result = memory.Span[0];
            memory = memory.Slice(1);
            return result;
        }
    }
}
