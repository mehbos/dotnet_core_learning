namespace Dotnet.Core.Learning.ExtensionOnRefStructs
{
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
    public static class RefStructExtensions
    {
        public static ref Point ChangeX(this ref Point point, int x)
        {
            point.X = x;
            return ref point;
        }
    }
}