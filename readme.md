# What is .NET core

.NET Core is a general purpose development platform maintained by Microsoft and the .NET community on GitHub. It is cross-platform, supporting Windows, macOS and Linux, and can be used in device, cloud, and embedded/IoT scenarios.

The following characteristics best define .NET Core:

- Flexible deployment: Can be included in your app or installed side-by-side user- or machine-wide.
- Cross-platform: Runs on Windows, macOS and Linux; can be ported to other OSes. The supported Operating Systems (OS), CPUs and application scenarios will grow over time, provided by Microsoft, other companies, and individuals.
- Command-line tools: All product scenarios can be exercised at the command-line.
- Compatible: .NET Core is compatible with .NET Framework, Xamarin and Mono, via the .NET Standard Library.
- Open source: The .NET Core platform is open source, using MIT an Apache 2 licenses. Documentation is licensed under CC-BY. .NET Core is a .NET Foundation project.
- Supported by Microsoft: .NET Core is supported by Microsoft, per .NET Core Support.

Basically, Microsoft built a version of .NET to allow developers to write cross-platform and cloud-optimized applications.

First, .NET is cross-platform. It runs on Windows, macOS and Linux, which allows the developer to share and run the exact same code between machines running different operating systems, with no changes in code and minimum or no changes in the development process. (Watch out for OS specific APIs!)

But the true innovation in .NET came with the modularization in design and architecture. The compiler (Roslyn) and the runtime (CoreCLR) are separate components that allow you to use different implementations (or even write your own).

Every library comes as a NuGet package, so when you start a new project, you don’t have any libraries, but a project file and a Program.cs. As you develop your app, you add libraries as you need them, allowing you to minimize the size of your application.

.NET Core also allows you to have multiple versions installed at the same time without having apps / parts of the OS breaking when you update or install a newer version of the framework, and will even allow you to ship the framework / parts of the framework with the application (since the footprint of the framework si small enough).

*.NET Core will favor performance with a standard library that minimizes allocations and the overall memory footprint of your system.*

## Versions

Versions of .NET Core available for download:

| Version      | Status      | Latest release | Latest release date | End of support |
|:------------:|:-----------:|:--------------:|:-------------------:|:--------------:|
|.NET Core 3.0 | Preview     | 3.0.0-preview7 | 2019-07-23          |                |
|.NET Core 2.2 | Current     | 2.2.6          | 2019-07-24          |                |
|.NET Core 2.1 | LTS         | 2.1.12         | 2019-07-24          |                |
|.NET Core 2.0 | End of life | 2.0.9          | 2018-07-10          | 2018-10-01     |
|.NET Core 1.1 | End of life | 1.1.13         | 2019-05-14          | 2019-06-27     |
|.NET Core 1.0 | End of life | 1.0.16         | 2019-05-14          | 2019-06-27     |

## How to install

- On windows download installer from [Microsoft](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.1.801-windows-x64-installer)
- On MacosX download installer from [Microsoft](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.1.801-macos-x64-installer)
- On linux:

    ```bash
    wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    sudo dpkg -i packages-microsoft-prod.deb
    sudo add-apt-repository universe
    sudo apt-get install apt-transport-https
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

    If you receive an error message similar to Unable to locate package dotnet-sdk-2.1, run the following commands.

    ```bash
    sudo dpkg --purge packages-microsoft-prod && sudo dpkg -i packages-microsoft-prod.deb
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

    If that doesn't work, you can run a manual install with the following commands.

    ```bash
    sudo apt-get install -y gpg
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
    sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
    wget -q https://packages.microsoft.com/config/ubuntu/18.04/prod.list
    sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
    sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
    sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list
    sudo apt-get install -y apt-transport-https
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1
    ```

## dotnet-cli

While visual studio is perfectly well with .NET core, some tasks can only executed in with command line. *for example creating native code*.

Popular commands:

- -h|--help: Show command, options and their meaning and parameters.
- --version: Show version of the dotnet
- new &lt;template&gt;: Create new something. This something will be indicated with name of the template.

  - --list: List all installed templates.
  - console: Console project.
  - classlib: .NETStandard class library project.
  - xunit: xUnit Test Project.
  - web: ASP.NET Core Empty.
  - ... and many many more.

- restore: restore nugets for the project and build it.
- build: build the ptoject you may use `-r <RID> -c <Configuration>` to build for an specific platform

## Topics

### ref struct

- Extension methods on ref structs

  ```cs --source-file code/ExtensionOnRefStructs/Point.cs --project code/ExtensionOnRefStructs/ExtensionOnRefStructs.csproj
  ```

  ```cs --region ExtensionOnStructs --source-file code/ExtensionOnRefStructs/Program.cs --project code/ExtensionOnRefStructs/ExtensionOnRefStructs.csproj
  ```

- System.Memory Nuget package and System.Buffer namespace:

  [This NuGet package](./docs/System.Memory.md) contains a set of helper structs and types that let you work with buffers in a high performance way.

- ValueTask&lt;T>

  [ValueTask](./docs/ValueTask.md) is a new performance related type that included in .NET core 2.0.

### System.IO.Pipes

Working with stream of bytes can be hard. [System.IO.Pipes](./docs/System.IO.Pipes.md) is a helper nuget that let you work with this problem and try to simplify the process.

## Working this Containers

Read more [here](./docs/Containers.md)
