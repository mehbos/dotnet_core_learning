# Working with containers

## What is a container

The word container refer to multiple things and it make it confusable.
Consider we have an OS, inside that OS we have multiple processes. Now if we want to isolate a process, what should we do?

This is basic definition of a container: It is a sand box for the process. We want to be able to limit this process, it can only see what is in the sand box.

  ```Text
  A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings.
  ```

Container may have multiple processes, it can run new processes but typically it has a main process and it is tightly coupled with its main process, when it end container will end.

## Going back in time

Let’s take a look at why container is so useful by going back in time.

![Container Evolution](images/container_evolution.svg)

**Traditional deployment era**: Early on, organizations ran applications on physical servers. There was no way to define resource boundaries for applications in a physical server, and this caused resource allocation issues. For example, if multiple applications run on a physical server, there can be instances where one application would take up most of the resources, and as a result, the other applications would underperform. A solution for this would be to run each application on a different physical server. But this did not scale as resources were underutilized, and it was expensive for organizations to maintain many physical servers.

**Virtualized deployment era**: As a solution, virtualization was introduced. It allows you to run multiple Virtual Machines (VMs) on a single physical server’s CPU. Virtualization allows applications to be isolated between VMs and provides a level of security as the information of one application cannot be freely accessed by another application.

Virtualization allows better utilization of resources in a physical server and allows better scalability because an application can be added or updated easily, reduces hardware costs, and much more.

Each VM is a full machine running all the components, including its own operating system, on top of the virtualized hardware.

**Container deployment era**: Containers are similar to VMs, but they have relaxed isolation properties to share the Operating System (OS) among the applications. Therefore, containers are considered lightweight. Similar to a VM, a container has its own filesystem, CPU, memory, process space, and more. As they are decoupled from the underlying infrastructure, they are portable across clouds and OS distributions.

Containers are becoming popular because they have many benefits. Some of the container benefits are listed below:

- Agile application creation and deployment: increased ease and efficiency of container image creation compared to VM image use.
- Continuous development, integration, and deployment: provides for reliable and frequent container image build and deployment with quick and easy rollbacks (due to image immutability).
- Dev and Ops separation of concerns: create application container images at build/release time rather than deployment time, thereby decoupling applications from infrastructure.
- Observability not only surfaces OS-level information and metrics, but also application health and other signals.
- Environmental consistency across development, testing, and production: Runs the same on a laptop as it does in the cloud.
- Cloud and OS distribution portability: Runs on Ubuntu, RHEL, CoreOS, on-prem, Google Kubernetes Engine, and anywhere else.
- Application-centric management: Raises the level of abstraction from running an OS on virtual hardware to running an application on an OS using logical resources.
- Loosely coupled, distributed, elastic, liberated micro-services: applications are broken into smaller, independent pieces and can be deployed and managed dynamically – not a monolithic stack running on one - big single-purpose machine.
- Resource isolation: predictable application performance.
- Resource utilization: high efficiency and density

## Container image

When we are speaking about container we usually refer to container image. It is a binary image of a container.

Container images become containers at runtime. Containerized software will always run the same, regardless of the infrastructure. Containers isolate software from its environment and ensure that it works uniformly despite differences for instance between development and staging.

Container image usually have a layering. The root layer is named *Scratch*. It is an empty container, we may think about it as an empty file system. On top of it we have an OS image like `Ubuntu` or `CentOs`. And on top of it we have processes and services that we use for our application.
This layering also let us handle errors and changes in a more managable way. We can fix a base layer and redeploy it and be sure that every thing else that use that layer work correctly.

*Note* In a normal code, when we work with a dependency like MSSQL, it is installed on the OS but in containers it is not installed. It just see above its base container image. So if we delete the container's image all of its software and dependency is gone.

## Docker

Docker containers that run on Docker Engine:

- Standard: Docker created the industry standard for containers, so they could be portable anywhere
- Lightweight: Containers share the machine’s OS system kernel and therefore do not require an OS per application, driving higher server efficiencies and reducing server and licensing costs
- Secure: Applications are safer in containers and Docker provides the strongest default isolation capabilities in the industry

## Dockerfile

In docker we build container images from a file named **Dockerfile**. It is a text file.
It should always start with a `FROM` that indicate our base image and then we want to install our dependencies to the docker image

```Dockerfile
FROM alpine:latest

#MAINTAINER mehdi.boss@gmail.com
LABEL maintainer="mehdi.boss@gmail.com" title="DockerfileTest"

RUN apk update
RUN apk add python3
```

## Container Registry

A registry is a thing that contains container images. You can pull and push images from registry.
Remember that when you pull and push from registry you only pull and push layers that you are missing, because locally you have an image cache.

## Docker client

In docker we have a client that work with a daemon (service in windows) that manage lifetime of the container images and running containers.

It also manage infrastructure of the host like networking, storage and things like that.

Most used container commands:

- builder:
  Will be used to manage builds. Sub commands are:
  - build:

    - Aliases: docker build | docker image build
    - Syntax: docker builder build [ OPTIONS ] PATH | URL | -
    - Important options:
      - -f | --file string: Name of the docker file. Default is 'PATH/Dockerfile'
      - -t | --tag string: Name and optional tag that should assigned to an image in form 'name:tag'.

- image:
  Will be used to work with images. Sub commands are:
  - ls:
    Aliases: docker images, docker image list
    Show list of images in the image cache
    Important options:
    - -a | --all: Show all images (default is to hide intermediate images)
  - inspect:
    Show content of an image.
  - pull:
    Syntax: docker image pull [ OPTIONS ] NAME[ :TAG ] [ @DIGEST ]
    Pull an image or a repository from a registry.
    Important options:
    - -a | --all-tags: Download all tagged images in the repository.
  - push:
    Syntax: docker image push [ OPTIONS ] NAME[ :TAG ]
    Push an image or a repository to a registry.
  - rm:
    Aliases: docker rmi, docker image remove
    Remove one or more images.
    Important options:
    - -f | --force: Force removal of the image.
  - tag:
    Syntax: docker image tag SOURCE_IMAGE[ :TAG ] TARGET_IMAGE[ :TAG ]
    Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE.
  - save:
    Syntax: docker image save [ OPTIONS ] IMAGE [ IMAGE... ]
    Save one or more images to a tar archive or STDOUT.
    Important options:
    - -o | --output string: Write to a file instead of STDOUT.
  - load:
    Syntax: docker image load [ OPTIONS ]
    Load an image from a tar archive or STDIN.
    Important options:
    - -i | --input string: Read from tar archive file, instead of STDIN.
  - prune:
    Remove unused images.
    Important options:
    - -a | --all: Remove all unused images instead of dangling ones.
    - -f | --force: Do not prompt for confirmation.

- container:
  - attach:
    Attach local standard input, output, and error streams to a running container
  - commit:
    Create a new image from a container's changes
  - cp:
    Syntax: docker cp [ OPTIONS ] CONTAINER:SRC_PATH DEST_PATH|-
            docker cp [ OPTIONS ] SRC_PATH|- CONTAINER:DEST_PATH
    Copy files/folders between a container and the local filesystem
    Use '-' as the source to read a tar archive from STDIN and extract it to a directory destination in a container.
    Use '-' as the destination to stream a tar archive of a container source to stdout.
  - create:
    Syntax: docker container create [ OPTIONS ] IMAGE [ COMMAND ] [ ARG... ]
    Create a new container.
    Important options:
    - -a | --attach list: Attach to STDIN, STDOUT or STDERR.
    - -e | --env list: Set environment variables.
    - --env-file list: Read in a file of environment variables.
    - -h | --hostname string: Container host name.
    - -i | --interactive: Keep STDIN open even if not attached.
    - -l | --label list: Set meta data on a container.
    - -p | --publish list: Publish a container's port(s) to the host.
    - --rm: Automatically remove the container when it exits.
    - -t | --tty: Allocate a pseudo-TTY.
    - -u | --user string: Username or UID (format: &lt;name|uid&gt;[ :&lt;group|gid&gt; ])
    - -v | --volume list: Bind mount a volume.
  - exec:
    Syntax: docker container exec [ OPTIONS ] CONTAINER COMMAND [ ARG... ]
    Run a command in a running container.
    Important options:
    - -d | --detach: Detached mode: run command in the background.
    - -e | --env list: Set environment variables.
    - -i | --interactive: Keep STDIN open even if not attached.
    - --privileged: Give extended privileges to the command.
    - -t | --tty: Allocate a pseudo-TTY.
    - -u | --user string: Username or UID (format: &lt;name|uid>[ :&lt;group|gid> ])
    - -w | --workdir string: Working directory inside the container
  - rm :
    Syntax: docker container rm [ OPTIONS ] CONTAINER [ CONTAINER... ]
    Remove one or more containers
    - -f | --force: Force the removal of a running container (uses SIGKILL).
    - -v | --volumes: Remove the volumes associated with the container.

## Working with data in container

Container's data is like the container, when you stop the container its data will be deleted. So if you want to have persistent data you must have a volume.
