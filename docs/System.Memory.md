# System.Memory

## Span&lt;T>/ReadOnlySpan&lt;T>:

  Is an array like type that allows representing managed and unmanaged memory in a uniform way and supports cheap slicing. It's at the heart of most performance-related improvements in .NET Core 2.1 and allows managing buffers in a more efficient way as it helps in reducing allocations and copying.
  *Span&lt;T>* is considered a core type and requires runtime and compiler support in order to be fully leveraged. Please note that when we refer to *Span&lt;T>* in this document, we really mean the family of span-related features which includes other types such as *ReadOnlySpan&lt;T>*, *Memory&lt;T>*, *ReadOnlyMemory&lt;T>*, and the new *System.Buffers* namespace.
  It represent contiguous regions of arbitrary memory, with performance characteristics on par with *T[]*. Its APIs are similar to the array, but unlike arrays, it can point to either managed or native memory, or to memory allocated on the stack.

  ```csharp
  public ref struct Span<T> {
      public Span(T[] array);
      public Span(T[] array, int start, int length);
      public unsafe Span(void* pointer, int length);

      public static implicit operator Span<T> (ArraySegment<T> arraySegment);
      public static implicit operator Span<T> (T[] array);

      public int Length { get; }
      public ref T this[int index] { get; }

      public Span<T> Slice(int start);
      public Span<T> Slice(int start, int length);
      public bool TryCopyTo(Span<T> destination);

      public T[] ToArray();
  }
  ```

  It has two different implementations:

  - Fast Span&lt;T> ( available on runtimes with special support for spans ):
    The fast implementation, will rely on "ref field" support and will look as follows:

    ```csharp
    public ref struct Span<T> {
        internal ref T _pointer;
        internal int _length;
    }
    ```

    A prototype of such fast *Span&lt;T>* can be found here. Through the magic of the *ref field*, it can support slicing without requiring a strong pointer to the root of the sliced object. The **GC** is able to trace the interior pointer, keep the root object alive, and update the interior pointer if the object is relocated during a collection.

  - Slow Span&lt;T> ( available on all current .NET runtimes, even existing ones, e.g. .NET 4.5 ):
    A different representation implemented for platforms that don’t support ref fields (interior pointers):

    ```csharp
    public struct Span<T> {
        internal IntPtr _pointer;
        internal object _relocatableObject;
        internal int _length;
    }
    ```

### Problem: Struct Tearing

  Struct tearing is a threading issue that affects all structs larger than what can be atomically updated on the target processor architecture. For example, some 64-bit processors can only update one 64-bit aligned memory block atomically. This means that some processors won’t be able to update both the _pointer and the _length fields of the Span atomically. This in turn means that the following code, might result in another thread observing _pointer and _length fields belonging to two different spans (the original one and the one being assigned to the field):

  ```csharp
  internal class Buffer {
      int _length;
      byte[] _memory;
      public Buffer(int cap) {
          Reset(new byte[cap]);
      }
      public Buffer(byte[] newMemory, int newLength) {
          Reset(newMemory, newLength);
      }

      public void Reset(byte[] newMemory, int newLength = 0) {
          // this update is not atomic. If atomicity is required, you must use some kind of lock
          _memory = newMemory;
          _length = newLength;
      }

      public int Size => _length;
      public int Capacity => _memory.Length;
      public byte this[int index] => _memory[index]; // this might see partial update
  }
  ```

  For most structs, tearing is at most a correctness bug and can be dealt with by making the fields (typed as the tearable struct type) non-public and synchronizing access to them. But since Span needs to be as fast as the array, access to the field cannot be synchronized. Also, because of the fact that Span accesses (and writes to) memory directly, having the _pointer and the _length be out of sync could result in memory safety being compromised.

  The only other way (besides synchronizing access, which would not be practical) to avoid this issue is to make Span a stack-only type, i.e. its instances can reside only on the stack (which is accessed by one thread).

### Span&lt;T> is stack-only

  *Span&lt;T>* is stack-only type; more precisely, it will be a by-ref type (just like its field in the fast implementation). This means that Spans cannot be boxed, cannot appear as a field of a non-stack-only type, and cannot be used as a generic argument. However, *Span&lt;T>* can be used as a type of method arguments or return values.

  *Span&lt;T>* being stack-only solves several problems:

  - Efficient representation and access: *Span&lt;T>* can be just managed pointer and length.
  - Efficient GC tracking: limit number of interior pointers that the GC have to track. Tracking of interior pointers in the heap during GC would be pretty expensive.
  - Safe concurrency (struct tearing discussed above): *Span&lt;T>* assignment does not have to be atomic. Atomic assignment would be required for storing *Span&lt;T>* on the heap to avoid data tearing issues.
  - Safe lifetime: Safe code cannot create dangling pointers by storing it on the heap when *Span&lt;T>* points to unmanaged memory or stack memory. The unsafe stack frame responsible for creating unsafe Span is responsible for ensuring that it won’t escape the scope.
  - Reliable buffer pooling: buffers can be rented from a pool, wrapped in spans, the spans passed to user code, and when the stack unwinds, the program can reliably return the buffer to the pool as it can be sure that there are no outstanding references to the buffer.

  The fast representation makes the type instances automatically stack-only, i.e. the constraint will be enforced by the CLR. This restriction should also be enforced by managed language compilers and/or analyzers for better developer experience. For the slow *Span&lt;T>*, language compiler checks and/or analyzers is the only option (as the runtimes won't enforce the stack-only restriction).

## Memory&lt;T>/ReadOnlyMemory&lt;T>:

A stack-only type with the associated trade-offs is great for low level developers writing data transformation routines. Productivity developers, writing apps, may not be so thrilled when they realize that when using stack-only types, they lose many of the language features they rely on to get their jobs done (e.g. async/await). And so, a stack-only type simply can’t be the primary exchange type we recommend for high level developers/scenarios/APIs.

For productivity developers we have *Memory&lt;T>*, that can be used with the full power of the language, i.e. it’s not stack-only. *Memory&lt;T>* can be seen as a “promise” of a Span. It can be freely used in generics, stored on the heap, used with async/await, and all the other language features we all love. When *Memory&lt;T>* is finally ready to be manipulated by a data transformation routine, it will be temporarily converted to a span (the promise will be realized), which will provide much more efficient (remember "on par with array") access to the buffer's data.

We have 2 kind of the *Memory&lt;T>*:

```cs --region UseMemory --source-file ../code/Memory/Program.cs --project ../code/Memory/Memory.csproj
```

- Unowned memory:

  ```cs --region UnownedMemory --source-file ../code/Memory/Program.cs --project ../code/Memory/Memory.csproj
  ```

- Owned memory:

  ```cs --region OwnedMemory --source-file ../code/Memory/Program.cs --project ../code/Memory/Memory.csproj
  ```

  As you see in this examples memory that returned from [MemoryPool.Shared.Rent](https://docs.microsoft.com/en-us/dotnet/api/system.buffers.memorypool-1.rent?view=netstandard-2.1#System_Buffers_MemoryPool_1_Rent_System_Int32_) is not required to have exact length as requested. But it has at least requested length.
  So if you require exact length you must use [Slice](https://docs.microsoft.com/en-us/dotnet/api/system.memory-1.slice?view=netstandard-2.1#System_Memory_1_Slice_System_Int32_) method to get required slice of the memory.

You may access items of this objects using item [Span](https://docs.microsoft.com/en-us/dotnet/api/system.memory-1.span?view=netstandard-2.1#System_Memory_1_Span) property.

```cs --region MemorySlice --source-file ../code/Memory/Program.cs --project ../code/Memory/Memory.csproj
```

## ReadOnlySequence&lt;T>:

A sequence of *Memory&lt;T>*

```cs --region WorkingWithSequences --source-file ../code/Sequence/Program.cs --project ../code/Sequence/Sequence.csproj
```

## Tensor&lt;T>:

Tensor&lt;T> can be thought of as extending *Memory&lt;T>* for multiple dimensions and sparse data. Primary attributes for defining a *Tensor&lt;T>* include:

- Type T: Primitive (e.g. int, float, string…) and non-primitive types
- Dimensions: The shape of the Tensor (e.g. 3×5 matrix, 28x28x3, etc.)
- Layout (Optional): Row vs. Column major. The API calls this “ReverseStride” to generalize to many dimensions instead of just “row” and “column”. The default is reverseStride: false (i.e. row-major).
- Memory (Optional): The backing storage for the values in the Tensor, which is used in-place without copying.

```csharp
public static Tensor<float> ConvertImageToFloat(Bitmap bitmap)
{
    Tensor<float> data = new DenseTensor<float>(new[] { bitmap.Width, bitmap.Height, 3 });
    for (int x = 0; x < bitmap.Width; x++)
    {
        for (int y = 0; y < bitmap.Height; y++)
        {
            Color color = bitmap.GetPixel(x, y);
            data[x, y, 0] = color.B;
            data[x, y, 1] = color.G;
            data[x, y, 2] = color.R;
        }
    }
    return data;
}
```

[Back To Home](../readme.md)
