# Microservices + Events + Containers

## Agenda

Main decisions to do in designing an application

* Monolith vs. Microservice
* Event-driven microservices
* Developing and deploying microservices using containers

## Successfull software development

![Application Design Constraints](images/application-design-constraints1.svg)

## The monolithic architecture

![The monolithic architecture](images/store-monolith-design.svg)

Simple to(In the begining):

* Develop
* Test
* Deploy
* Scale

![Monolithic architecture drawbacks](images/application-design-constraints2.svg)

After a while code grew and because everyone is contributing to same code base:

* Merging is not simple.
* Every change require a lot of talk with every other member to ensure that nothing breaks, so:

  * Chaging is not simple so there is no agility.
  * so autonomy goes away.

## Microservice architecture

![The microservice architecture](images/store-microservice-design.svg)

Each service will be developed and deployed separately, so:

* Agility is at highest.
* Autonomy is at its highest.
* You can select different technology for each service.
* You can scale each service independently of the others.

Drawbacks:

* Complexity

  * Complexity of developing a distributed system.

    * Implementing interprocess communication.
    * Handling partial failures.

  * Complexity of handling database transactions that span multiple databases (without 2PC).
  * Complexity of testing a distributed system.
  * Complexity of deploying and operating a distributed system.
  * Managing the developement and deployment of features that span multiple services.

But unlike monolithic architecture in which we had no solution to our problems, here there is solution for almost all of the problems.
So in general for large complex applications, benefits outweight drawbacks.

## Database designs

![Shared database design](images/store-monolith-db.svg)

Benefits:

* Simple and ACID
* Easy for DBAs and operations

Drawbacks(Because of tight coupling):

* Changes are difficult and require communication with everyone that access that data.
* After a while you loose track of who use what tables, so changing database become almost impossible

![Database per service design](images/store-database-per-service-db.svg)

Benefits(Because of loose coupling):

* Each service is able to change its database easily.
* Different databases may have different providers(like NoSQL, ...)

Drawbacks:

* More complex
* DBAs and operations must handle more databases.

**Problem:** How I maintain data consistency using this model?
**Answer:** Use event architecture to solve this.

![Event driven database model](images/event-driven-order-placement.svg)
